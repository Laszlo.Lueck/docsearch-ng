﻿FROM laszlo/containerruntimeglobal_x64_angular:23.04_7.0.10 as build
RUN mkdir -p /usr/local/app/docsearch-ng
WORKDIR /usr/local/app/docsearch-ng

COPY . .

RUN npm install --legacy-peer-deps
RUN npm run build --configuration=production

FROM nginx:alpine

COPY --from=build /usr/local/app/docsearch-ng/dist /usr/share/nginx/html
COPY /external/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
