

export interface JobRequest {
  jobName: string;
  groupId: string;
}
