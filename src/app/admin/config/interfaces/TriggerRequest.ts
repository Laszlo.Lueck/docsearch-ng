export interface TriggerRequest {
  triggerId: string;
  groupId: string;
}
