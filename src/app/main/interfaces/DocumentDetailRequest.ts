export interface DocumentDetailRequest {
  id: string,
  indexName: string
}
